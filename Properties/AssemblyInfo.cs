﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("KeraLua (.NET Framework 4.0)")]
[assembly: AssemblyDescription("Binding library for native Lua. This is an unofficial C#4.0 version fork by Jackalsoft Games, and future development is not guaranteed. Please support the official release!")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NLua")]
[assembly: AssemblyProduct("KeraLua")]
[assembly: AssemblyCopyright("Copyright © Vinicius Jarina 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a7a50781-1381-4697-904a-51426a67612f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.16.0")]
[assembly: AssemblyFileVersion("1.0.16.0")]
